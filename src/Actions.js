export const loadDataRequest = () => {
    return {
        type: 'LOAD_DATA_REQUEST'
    }
}

export const loadDataSuccess = (res) => {
    return {
        type: 'LOAD_DATA_SUCCESS',
        data: res['data']
    }
}

export const loadUADataRequest = () => {
    return {
        type: 'LOAD_UA_DATA_REQUEST'
    }
}

export const loadUADataSuccess = (res) => {
    return {
        type: 'LOAD_UA_DATA_SUCCESS',
        data: res['data']
    }
}
