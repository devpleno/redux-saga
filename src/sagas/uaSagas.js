import { put, call } from 'redux-saga/effects'
import { loadUADataSuccess } from '../Actions'

function* getUA(axios) {
    const dados = yield call(axios.get, 'http://httpbin.org/user-agent')
    yield put(loadUADataSuccess(dados))
}

export default getUA