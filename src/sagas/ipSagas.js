import { put, call } from 'redux-saga/effects'
import { loadDataSuccess } from '../Actions'

function* getIP(axios) {
    const dados = yield call(axios.get, 'http://httpbin.org/ip')
    yield put(loadDataSuccess(dados))
}

export default getIP