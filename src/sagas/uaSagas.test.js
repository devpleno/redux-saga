import uaSagas from './uaSagas'
import sagaHelper from 'redux-saga-testing'
import { call, put } from 'redux-saga/effects'
import { loadUADataSuccess } from '../Actions'

describe('should test uaSaga', () => {
    const axiosMock = {
        get: jest.fn()
    }

    const it = sagaHelper(uaSagas(axiosMock))

    it('should call', result => {
        expect(result).toEqual(call(axiosMock.get, 'http://httpbin.org/user-agent'))

        return {
            data: {
                'user-agent': 'ua chamado'
            }
        }
    })

    it('should call load', result => {
        console.log(result)
        expect(result).toEqual(put(loadUADataSuccess({'data' : {"user-agent": "ua chamado"}})))
    })
})