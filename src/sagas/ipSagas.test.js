import ipSagas from './ipSagas'
import sagaHelper from 'redux-saga-testing'
import { call, put } from 'redux-saga/effects'
import { loadDataSuccess } from '../Actions'

describe('should test ipSaga', () => {
    const axiosMock = {
        get: jest.fn()
    }

    const it = sagaHelper(ipSagas(axiosMock))

    it('should call', result => {
        expect(result).toEqual(call(axiosMock.get, 'http://httpbin.org/ip'))

        return {
            data: {
                origin: 'ip chamado'
            }
        }
    })

    it('should call load', result => {
        console.log(result)
        expect(result).toEqual(put(loadDataSuccess({'data' : {"origin": "ip chamado"}})))
    })
})