import React from 'react'
import { loadDataRequest } from './Actions'
import { connect } from 'react-redux'

const Info = (props) => {
    return (
        <div>
            <h2>Info</h2>

            <p>
            {props.data['origin']}
            </p>

            {!props.isFetching &&
                <button onClick={()=>props.loadData()}>Disparar getIP</button>
            }

            {props.isFetching &&
                <span>Carregando...</span>
            }
        </div>
    )
}

// class Info extends React.Component {
//     render() {
//         return (
//             <div>
//                 <h2>Info</h2>

//                 <p>
//                 {this.props.data['origin']}
//                 </p>

//                 {!this.props.isFetching &&
//                     <button onClick={()=> this.props.loadData()}>Disparar getIP</button>
//                 }

//                 {this.props.isFetching &&
//                     <span>Carregando...</span>
//                 }
//             </div>
//         )
//     }
// }

const mapStateToProps = (state) => {
    return {
        isFetching: state.ip.isFetching,
        data: state.ip.data,
        error: state.ip.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadData: () => dispatch(loadDataRequest())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Info)