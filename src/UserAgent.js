import React from 'react'
import { loadUADataRequest } from './Actions'
import { connect } from 'react-redux'

class UserAgent extends React.Component {
    render() {
        return (
            <div>
                <h2>UserAgent</h2>

                <p>
                {this.props.data['user-agent']}
                </p>

                {!this.props.isFetching &&
                    <button onClick={()=> this.props.loadData()}>Disparar getUA</button>
                }

                {this.props.isFetching &&
                    <span>Carregando...</span>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isFetching: state.ua.isFetching,
        data: state.ua.data,
        error: state.ua.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadData: () => dispatch(loadUADataRequest())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserAgent)