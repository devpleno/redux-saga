const initialState = {
    data: [],
    isFetching: false,
    error: false
}

const ua = (state = initialState, action) => {
    switch (action.type) {
        case "LOAD_UA_DATA_REQUEST":
            return {
                data: [],
                isFetching: true,
                error: false
            }
        case "LOAD_UA_DATA_SUCCESS":
            return {
                data: action.data,
                isFetching: false,
                error: false
            }
        case "LOAD_UA_DATA_ERROR":
            return {
                data: [],
                isFetching: false,
                error: true
            }
        default:
            return state
    }

}

export default ua